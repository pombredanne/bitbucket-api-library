<?php
/**
 * PHP 5.3 Bitbucket Api Library
 *
 * @copyright 2012 Provisionists, LLC
 * @license MIT
 * @version v0.1.6-rc
 * @author Anthony Steiner <asteiner@provisionists.com>
 */
namespace bitbucket\api;


/**
 * Abstract class for Api classes
 *
 * @author Anthony Steiner <asteiner@provisionists.com>
 * @package Bitbucket Api Library
 * @subpackage Core
 * @abstract
 */
abstract class ApiBase implements iApi
{


    /**
     * The core API
     * @var Api
     */
    protected $api;

    /**
     * Bitbucket API Abstract constructor
     * @param	Api	$api		Api object to load
     */
    public function __construct( Api $api )
    {
        $this->api = $api;
    }


    /**
     * Checks to see if the response is an object or raw output
     *
     * @param \bitbucket\api\Ambigous|object|mixed $response
     * @return boolean
     */
    public function IsResponseObject($response)
    {
        return (is_object( $response ) || is_array($response)) && $this->api->getOption( 'format' ) == Api_format::OBJECT;
    }


    /**
     * Descendant-usable username check.
     * If the username parameter is null it will return the authenticated user to the referenced parameter,
     * otherwise it uses the existing user
     * @param	string 	&$username	BitBucket user name
     * @final
     */
    public final function checkUsername( &$username = null )
    {
        $username = isset( $username ) ? $username : $this->api->getAuthenticatedUser();
    }

    /**
     * Checks the permission string for valid input
     * @param	string	$permission	The permission string from the invoking method
     * @final
     */
    public final function checkPermission( $permission )
    {
        $refclass = new \ReflectionClass( 'Api_permission' );
        $valid_permissions = $refclass->getConstants();

        if ( ! in_array( $permission, $valid_permissions ) )
        {
            echo sprintf( 'Permission is not valid (%s)', implode( ", ", $valid_permissions ) );
            exit();
        }
    }

    /**
     * Magic method when checking if the api isset()
     *
     * @param Ambiguous $api
     */
    public final function __isset( $api )
    {
        if ( ! isset( $api ) )
        {
            die( get_class( $api ) . " is not loaded!" );
        }
    }


}
