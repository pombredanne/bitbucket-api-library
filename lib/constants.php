<?php
/**
 * PHP 5.3 Bitbucket Api Library
 *
 * Use of this is optional, I'm using it as an ad-hoc enum (since PHP doesn't natively support Enumerations)
 * and I would like certain constants available from both Intellisense-enabled IDEs as well as being able to
 * edit their values from one place.
 *
 * Any where you see this class being used, the equivilent string may be used. Like I said; it's fully optional.
 *
 * @copyright 2012 Provisionists, LLC
 * @license MIT
 * @version v0.1.6-rc
 * @author Anthony Steiner <asteiner@provisionists.com>
 */
namespace bitbucket\api
{
    /**
     * API Permission enumeration
     *
     * @package Bitbucket Api Library
     * @subpackage Enums
     */
    class Api_permission
    {

        /**
         * No Permission
         * @var string
         */
        const NONE = 'none';


        /**
         * Read permission
         * @var string
         */
        const READ = 'read';


        /**
         * Read and Write permission
         * @var string
         */
        const WRITE = 'write';

        /**
         * Admin permission
         * @var string
         */
        const ADMIN = 'admin';
    }


    /**
     * API Object enumeration
     *
     * @package Bitbucket Api Library
     * @subpackage Enums
     */
    class Api_format
    {
        /**
         * API will return a PHP stdClass
         * @var string
         */
        const OBJECT = 'object';


        /**
         * API will return JSON-ready markup (via PHP's built-in function @see json_encode()
         * @var string
         */
        const JSON = 'json';

        /**
         * API will return XML
         * @var string
         */
        const XML = 'xml';

        /**
         * API will return YAML
         * @var string
         */
        const YAML = 'yaml';
    }


    /**
     * Read, write & admin enum common functions
     *
     * @package Bitbucket Api Library
     * @subpackage Enums
     */
    class enum_functions
    {
        /**
         * Default acceptable values
         * @var string[]
         */
        static private $_acceptable_values = array('read','write','admin');

        static private $_default = 'read';

        /**
         * Check the referenced enum value to see if it is a valid default value.
         * If not then default to "read"
         *
         * @param string $enum By-Reference
         * @return boolean
         */
        static public function check(&$enum)
        {
            if (!is_string($enum)) return self::$_default;
            $enum = in_array(strtolower($enum), self::$_acceptable_values) ? $enum : self::$_default;
            return in_array($enum, self::$_acceptable_values);
        }

        /**
         * Enter description here ...
         * @param array	$acceptable_values	Single-dimentional array of acceptable values
         * @param mixed	$default			Default Value, if check failes
         */
        public function __construct(array $acceptable_values = array(), $default = null)
        {
            if (!empty($acceptable_values))
            {
                self::$_acceptable_values = $acceptable_values;
            }

            if (!empty($default))
            {
                self::$_default = $default;
            }
        }
    }

    /**
     * Privilege Enum
     *
     * @package Bitbucket Api Library
     * @subpackage Enums
     */
    class privilege_enum extends enum_functions
    {

        const READ = 'read';

        const WRITE = 'write';

        const ADMIN = 'admin';

    }

    /**
     * Permission Enum
     *
     * @author Anthony Steiner <asteiner@provisionists.com>
     * @package Bitbucket Api Library
     * @subpackage Enums
     */
    class permission_enum extends privilege_enum{}

    /**
     * Users-Privileges Enum
     *
     * @author Anthony Steiner <asteiner@provisionists.com>
     * @package Bitbucket Api Library
     * @subpackage Enums
     */
    class users_privileges extends enum_functions
    {
        const WRITE = 'collaborator';

        const ADMIN = 'admin';

        public function __construct()
        {
            parent::__construct(array("collaborator", "admin"), "collaborator");
        }
    }


    /*
     * PLEASE DO NOT TOUCH THE BELOW DEFINES!!!
     *
     * If you want to override what these are using, simply define them in the file you're
     * creating this with before you include the bbApi.php file!
     *
     */
    if (!defined("BBAPI_DEFAULT_TIMEZONE"))
    {
        define("BBAPI_DEFAULT_TIMEZONE", "UTC");
    }

    if (!defined("TITLE_MISSING"))
    {
    	define("TITLE_MISSING", "(err 10013)");
    }
}