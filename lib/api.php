<?php
/**
 * PHP 5.3 Bitbucket Api Library
 *
 * @copyright 2012 Provisionists, LLC
 * @license MIT
 * @version v0.1.6-rc
 * @author Anthony Steiner <asteiner@provisionists.com>
 */
namespace bitbucket\api;

// Prerequisite
require_once 'iapi.php';
require_once 'api_base.php';
require_once 'constants.php';
require_once 'helpers.php';

/**
 * Master Api Class
 *
 * @author Anthony Steiner <asteiner@provisionists.com>
 * @package Bitbucket Api Library
 * @subpackage Core
 *
 */
class Api implements iApi
{

/*----- Api Properties ---- */

    /**
     * Group Privileges API
     * @var GroupPrivileges
     */
    public $group_privileges;

    /**
     * Groups API
     * @var Groups
     */
    public $groups;

	/**
	 * Changeset API
	 * @var Changesets
	 */
	public $changesets;

	/**
	 * Email API
	 * @var Emails
	 */
	public $emails;

	/**
	 * Events API
	 * @var Events
	 */
	public $events;

	/**
	 * Followers API
	 * @var Followers
	 */
	public $followers;

	/**
	 * Invitations API
	 * @var Invitations
	 */
	public $invitations;

	/**
	 * Issues API
	 * @var Issues
	 */
	public $issues;

	/**
	 * Privileges API
	 * @var Privileges
	 */
	public $privileges;

	/**
	 * Repositories API
	 * @var Repositories
	 */
	public $repositories;

	/**
	 * Services API
	 * @var Services
	 */
	public $services;

	/**
	 * Source API
	 * @var Source
	 */
	public $source;

	/**
	 * SSH Keys API
	 * @var SshKeys
	 */
	public $ssh_keys;

	/**
	 * Users API
	 * @var Users
	 */
	public $users;

	/**
	 * Authenticated User API
	 * @var User
	 */
	public $user;

	/**
	 * Wiki API
	 * @var Wiki
	 */
	public $wiki;

	/**
	 * New User
	 * @var Newuser
	 */
	public $newuser;
/*----- /END Api Properties ---- */


	/**
	 * Request object
	 * @access protected
	 * @var Request
	 */
	protected $request = null;


	/**
	 * Collection of API Objects
	 * @access protected
	 * @var array(ApiBase)
	 */
	protected $apis = array();

	/**
	 * Logical boolean whether or not to print debug information
	 * @access protected
	 * @var boolean
	 */
	protected $debug;

	/**
	 * Byr
	 * @var Api
	 * @static
	 */
	private static $instance;

	/**
	 * Default constructor
	 * @param Api_format|scalar $out_format @see Api_format Will accept Api_format::CONSTANTNAME or the equivalent string ('object', 'json', 'xml', 'yaml')
	 * @param boolean $debug	Setting to true will return debug information along with the return
	 */
	public function __construct($username, $password, $out_format = Api_format::OBJECT , $debug = false)
	{
	    self::$instance =& $this;

		$this->setOption('format', $out_format);

		$this->debug = $debug;
		$this->getRequest()->setOption( 'debug', $debug );
        $this->authenticate($username, $password);
		$this->assign_apis();
	}

	/**
	 * ByRef Substantiation of the Api, used by sub APIs
	 * @return \bitbucket\api\Api
	 */
	public static function &get_instance()
	{
        return self::$instance;
	}

	/**
	 * Assign all APIs to their respective chain-segment
	 */
	public function assign_apis()
	{
		$refclass = new \ReflectionClass( $this );
		foreach( $refclass->getProperties(\ReflectionProperty::IS_PUBLIC) as $property )
		{
			$name = $property->name;
			if ( $property->class == $refclass->name )
			{
				$this->{$property->name} = $this->getApi($property->name);
			}
		}
	}

	/**
	 * Removes the underscore and CamelCases the classes to fit the standard class naming uses for this API lib
	 * @param string $class_name Class Name to sanitize for this API lib
	 *
	 * @return string
	 */
	public function sanitize_classname($class_name)
	{
		$sanitized_arr =  array_map("ucfirst", explode("_", $class_name));
	    return implode("", $sanitized_arr);
	}

	/**
	 * Reflection class to help make the API more dynamic. Retreives the public properties of class name
	 * @param scalar $class
	 * @return StdClass
	 */
	public function get_classProperties( $class )
	{
		$class_properties = (object)array();

		$refclass = new \ReflectionClass( $class );
		foreach( $refclass->getProperties( \ReflectionProperty::IS_PUBLIC ) as $property )
		{
			$name = $property->name;
			if ( ($property->class == $refclass->name) )
			{
				$class_properties->{$property->name} = $class->{$property->name};
			}
		}

		return $class_properties;
	}


	/**
	 * Authenticates the entire session and any calls to the webservice made until deAuthenticate() is called
	 * @see Api::deAuthenticate()
	 * @param string $username	Your Bitbucket username or email
	 * @param string $password	Your Bitbucket pasword (password is encoded and sent securely to BB's secure api address)
	 * @return Api
	 */
	private function authenticate($username, $password)
	{
		$this->getRequest()->setOption( 'username', $username )->setOption( 'password', $password );

		return $this;
	}

	/**
	 * Returns a non-session webservice. At this point the web service is no longer authenticaed
	 * @return Api
	 */
	public function deAuthenticate()
	{
		return $this->authenticate( null, null );
	}

	/**
	 * Helper method for HTTP Method "GET" to retrieve data with the request
	 * @param	string		$route				A relative Bitbucket RESTFul uri route ({@link Request::$options })
	 * @param	array|string		$parameters			The data to send with the request
	 * @param	array		$requestOptions		Request options, other than the default options ({@link Request::$options })
	 * @return	Ambigous <object, mixed>		Ambigous object depending on the return format chosen
	 */
	public function get($route, $parameters = array(), array $requestOptions = array())
	{
		return $this->getRequest()->get( $route, $parameters, $requestOptions );
	}

	/**
	 * Helper method for HTTP Method "POST" to send presumably new data with the request
	 * @param	string		$route				A relative Bitbucket RESTFul uri route ({@link Request::$options })
	 * @param	array|string		$parameters			The data to send with the request
	 * @param	array		$requestOptions		Request options, other than the default options ({@link Request::$options })
	 * @return	Ambigous <object, mixed>		Ambigous object depending on the return format chosen
	 */
	public function post($route, $parameters = array(), array $requestOptions = array())
	{
		return $this->getRequest()->post( $route, $parameters, $requestOptions );
	}

	/**
	 * Helper method for HTTP Method "PUT" to update the data for the request
	 * @param	string		$route				A relative Bitbucket RESTFul uri route ({@link Request::$options })
	 * @param	array|string		$parameters			The data to send with the request
	 * @param	array		$requestOptions		Request options, other than the default options ({@link Request::$options })
	 * @return	Ambigous <object, mixed>		Ambigous object depending on the return format chosen
	 */
	public function put($route, $parameters = array(), array $requestOptions = array())
	{
		return $this->getRequest()->put( $route, $parameters, $requestOptions );
	}

	/**
	 * Helper method for HTTP Method "DELETE" to delete the data for the request
	 * @param	string		$route				A relative Bitbucket RESTFul uri route ({@link Request::$options })
	 * @param	array|string		$parameters			The data to send with the request
	 * @param	array		$requestOptions		Request options, other than the default options ({@link Request::$options })
	 * @return	Ambigous <object, mixed>		Ambigous object depending on the return format chosen
	 */
	public function delete($route, $parameters = array(), array $requestOptions = array())
	{
		return $this->getRequest()->delete( $route, $parameters, $requestOptions );
	}

	/**
	 * Reteives the authenticated username (if any) from the {@link Request::getAuthenticatedUser() set options}
	 * @return	string		Authenticated username
	 */
	public function getAuthenticatedUser()
	{
		return $this->getRequest()->getAuthenticatedUser();
	}

	/**
	 * Sets options for the request object cleanly
	 * @param string	$name	Option key
	 * @param mixed		$value	Option value
	 */
	public function setOption($name, $value)
	{
		$this->getRequest()->setOption($name, $value);
	}

	/**
	 * Gets an option by key, returns the default paramater in the event the option key doesn't exist
	 * @param string	$name		Option key
	 * @param mixed		$default	Default value to return if the option value is null
	 * @return Ambigous <mixed, multitype:>
	 */
	public function getOption($name, $default = null)
	{
		return $this->getRequest()->getOption($name, $default);
	}


	/**
	 * A clean way to get the current request context
	 * @return Request
	 */
	public function getRequest()
	{
		if ( ! isset( $this->request ) )
		{
			require_once (dirname( __FILE__ ) . '/request.php');
			$this->request = new Request( array('debug' => $this->debug));
		}

		return $this->request;
	}

	/**
	 * A clean way to set a brand new request object to the instantiated request
	 * @param Request $request
	 * @return Api
	 */
	public function setRequest(Request $request)
	{
		$this->request = $request;

		return $this;
	}

	/**
	 * Uses reflection from initializer method to make sure the API class exists and then loads it into the api property
	 * This allows the end-user to omit api files they don't need for better performance
	 *
	 * @param string $name	Property/API name to substantiate the api class in to
	 * @return array(ApiBase|NULL)
	 */
	public function getApi($api, $sub_api = null, &$class_name = null)
	{
        $class_lower = strtolower($api);
        $class_raw = $api;
        $is_sub = !empty($sub_api);

        if ($is_sub)
        {
            if (! isset( $this->apis[$class_lower][$sub_api] ) )
            {
            	$class_name = $this->sanitize_classname($sub_api);

            	$api_file = sprintf("/apis/$class_lower/$sub_api.php");

            	if ( ! file_exists(dirname( __FILE__ ) . $api_file))
            	{
            	    $this->apis[$class_lower][$sub_api] = null;
            	    return  $this->apis[$class_lower][$sub_api];
            	}

                require_once (dirname( __FILE__ ) . $api_file );
                if (!class_exists(__NAMESPACE__."\\$class_lower\\$class_name"))
                {
                    $this->apis[$class_lower][$sub_api] = null;
                    return $this->apis[$class_lower][$sub_api];
                }

                $class_name = "\\".__NAMESPACE__."\\$class_lower\\$class_name";

                $this->apis[$class_lower][$sub_api] = new $class_name( $this );

            }


    		return $this->apis[$class_lower][$sub_api];
        }
        else
        {

            if (! isset( $this->apis[$api] ) )
            {
            	$class_name = $this->sanitize_classname($api);

            	$api_file = sprintf("/apis/$api.php");

            	if ( ! file_exists(dirname( __FILE__ ) . $api_file)) { $this->apis[$api] = null; return $this->apis[$api]; }

                require_once (dirname( __FILE__ ) . $api_file );
                if (!class_exists(__NAMESPACE__."\\$class_name")) { $this->apis[$api] = null; return $this->apis[$api]; }

                $class_name = __NAMESPACE__."\\$class_name";

                $by_ref = $this;
                $this->apis[$api] = new $class_name( $by_ref );
            }

    		return $this->apis[$api];
        }
	}

	/**
	 * A clean way to set an api post-instantiate
	 *
	 * @param string $name Property/API name
	 * @param iApi $instance
	 * @return iApi
	 */
	public function setApi($name, iApi $instance)
	{
		$this->apis[$name] = $instance;

		return $this;
	}
}
