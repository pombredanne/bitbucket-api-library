<?php
/**
 * PHP 5.3 Bitbucket Api Library
 *
 * @copyright 2012 Provisionists, LLC
 * @license MIT
 * @version v0.1.6-rc
 * @author Anthony Steiner <asteiner@provisionists.com>
 */
namespace bitbucket\api;


/**
 * The groups endpoint provides functionality for querying information about groups,
 * creating new ones, updating memberships, and deleting them. Both individual and team
 * accounts can define groups.  To manage group information on an individual account,
 * the caller must authenticate with administrative rights on the account.
 * To manage groups for a team account, the caller must authenticate as a team member with administrative rights on the team.
 *
 * @author Anthony Steiner <asteiner@provisionists.com>
 * @package Bitbucket Api Library
 * @subpackage API.Groups
 */
class Groups extends ApiBase
{

    /**
     * Filters
     * @var Group_Filter
     */
    public $filters;

    /**
     * Class Constructor
     * @param bitbucket\api\IApi $api API object
     */
    public function __construct($api)
    {
        parent::__construct($api);
        $this->filters = new Group_Filter();
    }

    /**
     * Get a list groups matching one or more filters.
     * The caller must authenticate with administrative rights or as a group member to view a group.
     *
     * @param Group_Filter $filters A collection of filters
     * @return Ambigous <\bitbucket\api\Ambigous, object, mixed>
     */
    public function show(Group_Filter $filters = null)
    {
        if (!empty($filters))
        {
            $this->filters = $filters;
        }

        $response = null;
        $data = null;

        $data = $this->filters->get_http_data();

        $response = $this->api->get("groups", $data);

        return $response;
    }


    /**
     * Get a list of an account's groups.  The caller must authenticate with
     * administrative rights on the account or as a group member to view a group.
     *
     * @param string $account_name (Optional) The team or individual account name. You can supply a user name or a valid email address. Leave blank to use the authenticated user.
     * @return Ambigous <\bitbucket\api\Ambigous, object, mixed>
     */
    public function show_account($account_name = null)
    {
        $response = null;

        $this->checkUsername($account_name);

        $response = $this->api->get("groups/$account_name/");

        return $response;
    }

    /**
     * Creates a new group.  The caller must authenticate with administrative rights on an account to access its groups.
     *
     * @param string $group_name The name of new group.
     * @param string $account_name (Optional) The team or individual account name.  You can supply a user name or a valid email address.  Leave blank to use the authenticated user.
     * @return Ambigous <\bitbucket\api\Ambigous, object, mixed>
     */
    public function create($group_name, $account_name = null)
    {
        $response = null;

        $this->checkUsername($account_name);

        $data['name'] = $group_name;

        $data = array_filter($data);

        $response = $this->api->post("groups/$account_name/", $data);

        return $response;
    }

    /**
     * Updates an existing group resource. The caller must authenticate with administrative rights on the account.
     *
     * @param string $group_slug	The group's slug.
     * @param string $group_name	(Optional) The name of the group.
     * @param string $permission	(Optional) One of read, write, or admin.  It is also possible to use {@see \bitbucket\api\permission_enum}.  Leave blank to use "READ" permissions.
     * @param boolean $auto_add		(Optional) A boolean value. True to automatically add the group if it doesn't already exist.  Omit to not set.
     * @param string $account_name	(Optional) The team or individual account name. You can supply an account name or the primary email address for the account. Leave blank to use the authenticated user.
     * @return Ambigous <\bitbucket\api\Ambigous, object, mixed>
     */
    public function update($group_slug, $group_name = null, $permission = null, $auto_add = null, $account_name = null)
    {
        $response = null;
        $data = array();
        $header = array();
        $this->checkUsername($account_name);

        Helper::format_slug($group_slug);

        if (!empty($permission))
        {
            permission_enum::check($permission);
        }

        $data['name'] = $group_name;
        $data['permission'] = $permission;


        if (is_bool($auto_add))
        {
            $data['auto_add'] = $auto_add;
        }

        $data = array_filter($data);

        $response = $this->api->put("groups/$account_name/$group_slug/", json_encode($data));


        return $response;
    }


    /**
     * Deletes a group.  The caller must authenticate with administrative rights on the account.
     *
     * @param string $group_slug	The group's slug.
     * @param string $account_name	(Optional) The team or individual account name. You can supply an account name or the primary email address for the account. Leave blank to use the authenticated user.
     * @return boolean	If the request is successful it will return true, otherwise false
     */
    public function delete($group_slug, $account_name = null)
    {
        $response = null;
        $this->checkUsername($account_name);

        Helper::format_slug($group_slug);

        $this->api->delete("groups/$account_name/$group_slug");

        $response = $this->api->getRequest()->http_code == '204' ? true : false;

        return $response;
    }

    /**
     * Gets the membership for a group. The caller must authenticate with administrative rights on the account.
     *
     * @param string $group_slug	The group's slug.
     * @param string $account_name	(Optional) The team or individual account name. You can supply an account name or the primary email address for the account. Leave blank to use the authenticated user.
     * @return Ambigous <\bitbucket\api\Ambigous, object, mixed>
     */
    public function members($group_slug, $account_name = null)
    {
        $response = null;
        $this->checkUsername($account_name);

        Helper::format_slug($group_slug);

        $response = $this->api->get("groups/$account_name/$group_slug/members");

        return $response;
    }

    /**
     * Adds a member to a group. The caller must authenticate with administrative rights on an account to access its groups.
     *
     * @param string $member_name		Case Sensitive!  A individual account name. You can supply an account name or the primary email address for the account.
     * @param string $group_slug		The group's slug.
     * @param string $account_name	(Optional) The team or individual account name. You can supply an account name or the primary email address for the account. Leave blank to use the authenticated user.
     * @return Ambigous <\bitbucket\api\Ambigous, object, mixed>
     */
    public function add_member($member_name, $group_slug, $account_name = null)
    {
        $response = null;
        $this->checkUsername($account_name);

        Helper::format_slug($group_slug);

        $response = $this->api->put("groups/$account_name/$group_slug/members/$member_name", '{"username":"'.$member_name.'"}');

        return $response;
    }

    /**
     * Deletes a member from a group.  The caller must authenticate with administrative rights on the account.
     *
     * @param string $member_name		Case Sensitive! A individual account name. You can supply an account name or the primary email address for the account.
     * @param string $group_slug		The group's slug.
     * @param string $account_name		(Optional) The team or individual account name. You can supply an account name or the primary email address for the account. Leave blank to use the authenticated user.
     * @return boolean
     */
    public function delete_member($member_name, $group_slug, $account_name = null)
    {
        $response = null;
        $this->checkUsername($account_name);

        Helper::format_slug($group_slug);

        $this->api->delete("groups/$account_name/$group_slug/members/$member_name");
        $response = $this->api->getRequest()->http_code == '204' ? true : false;

        return $response;
    }
}





/**
 * This class will allow you to create groups filter objects on-the-fly
 * <code>
 * <?php
 * use bitbucket\api\Group_Filter;
 * // Using this methodolgy you'll be able to indefinitely chain filters to the object and pass the output to an API method.
 * $filters = Group_Filter::("accountname1", "group-slug1")->filter("accountname2", "group-slug2");
 * $result = $api->groups->show($filters);
 * print_r($result);
 * ?>
 * </code>
 * @author Anthony Steiner <asteiner@steinerd.com>
 *
 * @package Bitbucket Api Library
 * @subpackage API.Groups
 */
final class Group_Filter
{
    /**
     * Collection of filters
     * @var string[]
     */
    static private $filter_collection = array();

    /**
     * Build and return the HTTP data for a filter object request
     * @return string
     */
    public function get_http_data()
    {
        $post = urldecode(http_build_query(self::$filter_collection, "group~~", '&amp;'));
        $post = preg_replace('/group~~\d/i', "group", $post);
        return $post;
    }

    /**
     * Kick off the static object with a method signature matching the filter method
     *
     * @param string $ownername	Group owner name.
     * @param string $group_slug Group Slug or Name.
     */
    public function __construct($ownername = null, $group_slug = null)
    {
        if (!empty($ownername) && !empty($group_slug))
        {
            self::$filter_collection[] = self::clean($ownername, $group_slug);
        }

    }

    /**
     * A static method to be called that will automatically append the original static
     * object with itself aka fluent interfacing.
     *
     * @param string $ownername	Group owner name.
     * @param string $group_slug Group Slug or Name.
     * @return Group_Filter	A new instance of intself with each previous filter
     */
    static public function filter($ownername, $group_slug)
    {
        return new Group_Filter($ownername, $group_slug);
    }

    /**
     * Clean up and taxonomize the data request requirements
     *
     * @param string $ownername	Group owner name.
     * @param string $group_slug Group Slug or Name.
     * @return string
     */
    private function clean( $ownername, $group_slug)
    {
        Helper::format_slug($group_slug);
        return sprintf('%1$s/%2$s', $ownername, $group_slug);
    }
}

