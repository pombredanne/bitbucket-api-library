<?php
/**
 * PHP 5.3 Bitbucket Api Library
 *
 * @copyright 2012 Provisionists, LLC
 * @license MIT
 * @version v0.1.6-rc
 * @author Anthony Steiner <asteiner@provisionists.com>
 *
 */
namespace bitbucket\api\repositories\issues;

use \bitbucket\api\repositories\Issues;

/**
 * Encapsulates the members of the IssueFilter class.
 * Helps create a (sane) way to build an issue filter in PHP using fluent-interfacing
 *
 * @author Anthony Steiner <asteiner@provisionists.com>
 * @package Bitbucket Api Library
 * @subpackage "Issues" Sub-classes
 */
class IssueFilter
{
	/**
	 * Referenced repository api object
	 * @var Issues
	 */
	private $_issue_api;

	/**
	 * The base api reference
	 * @var Api
	 */
	private $_api;

	/**
	 * @var string[] Impromptu string-builder, string array
	 */
	public $stringBuilder = array();

	/**
	 * An integer specifying the number of issues to return. You can specify a value between 0 and 50. The default is 15.
	 * @var int
	*/
	public $limit = 15;

	/**
	 * Offset to start at. The default is 0 (zero).
	 * @var int
	 */
	public $start = 0;

	/**
	 * A string to search for.
	 * @var string
	 */
	public $search;

	/**
	 * Sorts the output by any of the metadata fields. Enter a values such as milestone
	 * @var string
	 */
	public $sort;

	/**
	 * Filter object for the issue title
	 * @var Filter
	 */
	public $title;

	/**
	 * Filter object for the content of an issue
	 * @var Filter
	 */
	public $content;

	/**
	 * A boolean based filter object for the repo version relavant to an issue
	 * @var FilterBinary
	 */
	public $version;

	/**
	 * A boolean based filter object for the repo milestone relavant to an issue
	 * @var FilterBinary
	 */
	public $milestone;

	/**
	 * A boolean based filter object for the repo component type relavant to an issue
	 * @var FilterBinary
	 */
	public $compontent;

	/**
	 * A boolean based filter object for the repo type relavant to an issue
	 * @var FilterBinary
	 */
	public $kind;

	/**
	 * A boolean based filter object for the repo component status relavant to an issue
	 * @var FilterBinary
	 */
	public $status;

	/**
	 * A boolean based filter object for the repo user relavant to an issue
	 * @var FilterBinary
	 */
	public $responsible;

	/**
	 * A filter object for the original ticket author relavant to an issue
	 * @var Filter
	 */
	public $reported_by;

	/**
	 * A boolean based filter object for the repo proirity relavant to an issue
	 * @var FilterBinary
	 */
	public $priority;

	/**
	 * Default constructor, designed to iterate through itself for public properies
	 * and initialize them as a Filter object... used the most populated filter class to avoid a greedy logic check
	 */
	public function __construct($issue_api = null, $api = null)
	{
		// Get Class & instantiate
		$refclass = new \ReflectionClass( $this );
		$name = "";

		if (!empty($issue_api) || !empty($issue_api) )
		{
			$this->_issue_api =& $issue_api;
			$this->_api = $api;
		}

		// Loop through the public properties
		foreach( $refclass->getProperties(\ReflectionProperty::IS_PUBLIC) as $property )
		{
			// Substantiate property name to a local
			$name = $property->name;

			if ( $property->class == $refclass->name && $name != 'stringBuilder')
			{
				$this->{$property->name} = new Filter($this, $property->name);
			}
		}
	}

	/**
	 * Force class to return the built filter string when type-boxed with a string scalar
	 */
	public function __toString()
	{
		return implode("&", $this->stringBuilder);
	}

	/**
	 * Quicker show-point clone for the filter class
	 * @param string $repo_slug		The repository identifier.
	 * @param string $account_name	The team or individual account owning the repository.
	 * @return \bitbucket\api\repositories\Ambigous
	 */
	final public function show($repo_slug, $account_name = null)
	{

		if ($this->_issue_api == null)
		{
			$this->_issue_api = new Issues($this->_api);
		}

		return $this->_issue_api->show($repo_slug, $account_name, $this);
	}
}

/**
 * Ecapsulates the members of the binary filter object
 *
 * @author Anthony Steiner <asteiner@provisionists.com>
 * @package Bitbucket Api Library
 * @subpackage "Issues" Sub-classes
 */
class FilterBinary
{
	/**
	 * String of the parent method for reflection purposes in the IssueFilter class constructor
	 * @var string
	 */
	private $parent_method;

	/**
	 * The parent class, for the automated fluent interfacing
	 * @var IssueFilter
	 */
	private $parent_class;

	/**
	 * Default constructor, handles the property substantiation for a more intuitive fluent-interface.
	 * @param IssueFilter $parent_class		The parent class, for the automated fluent interfacing
	 * @param string $parent_method			String of the parent method for reflection purposes in the IssueFilter class constructor
	 */
	public function __construct(&$parent_class, $parent_method)
	{
		$this->parent_method = $parent_method;
		$this->parent_class =& $parent_class;
	}

	/**
	 * Exact Match (=)
	 * @param string $filter_text
	 * @return IssueFilter
	 */
	public function is($filter_text)
	{
		return $this->_fluent_return("{$this->parent_method}=$filter_text");
	}

	/**
	 * Exact Exclusion (!=)
	 * @param string $filter_text
	 * @return IssueFilter
	 */
	public function is_not($filter_text)
	{
		return $this->_fluent_return("{$this->parent_method}=!$filter_text");
	}

	/**
	 * Helps build the string as a common method and keeps the result together
	 * @param string $filter
	 * @return IssueFilter
	 */
	protected function _fluent_return($filter)
	{
		$this->parent_class->stringBuilder[] = $filter;
		return $this->parent_class;
	}
}

/**
 * Encapsulates the Filter object methods
 *
 * @author Anthony Steiner <asteiner@provisionists.com>
 * @package Bitbucket Api Library
 * @subpackage "Issues" Sub-classes
 */
class Filter extends FilterBinary
{
	/**
	 * String of the parent method for reflection purposes in the IssueFilter class constructor
	 * @var string
	 */
	private $parent_method;

	/**
	 * The parent class, for the automated fluent interfacing
	 * @var IssueFilter
	 */
	private $parent_class;

	/**
	 * Default constructor, handles the property substantiation for a more intuitive fluent-interface.
	 * @param IssueFilter $parent_class		The parent class, for the automated fluent interfacing
	 * @param string $parent_method			String of the parent method for reflection purposes in the IssueFilter class constructor
	 */
	public function __construct(&$parent_class, $parent_method)
	{
		parent::__construct($parent_class, $parent_method);
		$this->parent_method = $parent_method;
		$this->parent_class =& $parent_class;
	}

	/**
	 * Filter subject contains filtertext (~)
	 * @param string $filter_text
	 * @return IssueFilter
	 */
	public function contains($filter_text)
	{
		return $this->_fluent_return("{$this->parent_method}=~$filter_text");
	}

	/**
	 * Filter subject does not contain filter text (!~)
	 * @param string $filter_text
	 * @return IssueFilter
	 */
	public function doesnt_contain($filter_text)
	{
		return $this->_fluent_return("{$this->parent_method}=!~$filter_text");
	}

	/**
	 * Filter subject begins with filter text (^)
	 * @param string $filter_text
	 * @return IssueFilter
	 */
	public function begins_with($filter_text)
	{
		return $this->_fluent_return("{$this->parent_method}=^$filter_text");
	}

	/**
	 * Filter sjubect ends with filter text ($)
	 * @param string $filter_text
	 * @return IssueFilter
	 */
	public function ends_with($filter_text)
	{
		return $this->_fluent_return("{$this->parent_method}=$$$filter_text");
	}

}