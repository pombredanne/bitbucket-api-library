<?php
/**
 * PHP 5.3 Bitbucket Api Library
 *
 * @copyright 2012 Provisionists, LLC
 * @license MIT
 * @version v0.1.6-rc
 * @author Anthony Steiner <asteiner@provisionists.com>
 *
 */
namespace bitbucket\api\repositories;

use \bitbucket\api\Repositories;

use \bitbucket\api\Helper;
use \bitbucket\api\Api;
use \bitbucket\api\ApiBase;

// For the Issues SHOW method
require('issues/filters.php');
use \bitbucket\api\repositories\issues\IssueFilter;
use \bitbucket\api\repositories\issues\Filter;
use \bitbucket\api\repositories\issues\FilterBinary;

// For the Issues CREATE method
require('issues/fields.php');
use \bitbucket\api\IssueFields;

/**
 * The issues resource provides functionality for getting information on issues in an issue tracker, creating new issues,
 * updating them and deleting them.
 * You can access public issues without authentication, but you will only receive a subset of information,
 * and you can't gain access to private repositories' issues.
 * By authenticating, you will get a more detailed set of information, the ability to create issues,
 * as well as access to updating data or deleting issues you have access to.
 *
 * @author Anthony Steiner <asteiner@provisionists.com>
 * @package Bitbucket Api Library
 */
class Issues extends ApiBase
{
	/**
	 * Filter object to aid with building a proper filter for the issue->show() method
	 * Example:
	 * <code>
	 * <?php
	 * $api = new Api( $username, $password );
	 *
	 * $res = $api->repositories->issues->filter
	 * 	->title->contains("Thing1")
	 * 	->title->contains("Thing2")
	 * 	->show( "catinthehat", "drseuss");
	 *
	 * print_r( $res );
	 * ?>
	 * </code>
	 * @var IssueFilter
	 */
	public $filter;

	/**
	 * Create fields for a new issue for the {@see create()}
	 * Example:
	 * <code>
	 * <?php
	 * $api = new Api( $username, $password );
	 *
	 * $res = $api->repositories->issues->fields
	 * 	->title("test")
	 * 	->status(Status::STATUS_NEW)
	 * 	->content("Issue description")
	 * 	->create("cateinthehat", "drseuss");
	 *
     * print_r( $res );
	 * ?>
	 * </code>
	 * @var \bitbucket\api\IssueFields
	 */
	public $fields;


	/**
	 * Default constructor, substantiates the filter property with a new IssueFilter object
	 * @param Api $api
	 */
	public function __construct($api)
	{
		parent::__construct($api);

		if (empty($this->filter))
		{
			$this->filter = new IssueFilter($this, $api);
		}

		if (empty($this->fields))
		{
			$this->fields = new IssueFields($this, $api);
		}
	}

	/**
	 * Gets the list of issues in the repository.  If you issue this call without filtering parameters, the count value contains the total number of issues in the repository's tracker.  If you filter this call, the count value contains the total number of issues that meet the filter criteria.
	 * Authorization is not required for public repositories with a public issue tracker. Private repositories or private issue trackers require the caller to authenticate with an account that has appropriate authorisation. By default, this call returns 15 issues. If necessary you can specify the sort parameter to order the output.
	 * Example:
	 * <code>
	 * <?php
	 * $api = new Api( $username, $password );
	 *
	 * $res = $api->repositories->issues->filter
	 * 	->title->contains("Thing1")
	 * 	->title->contains("Thing2")
	 * 	->show( "catinthehat", "drseuss");
	 *
	 * print_r( $res );
	 * ?>
	 * </code>
	 * @param string $repo_slug		The repository identifier.
	 * @param string $account_name	The team or individual account owning the repository.
	 * @param IssueFilter $filter	The chained method {@see \bitbucket\api\Repositories\IssueFilter} that allows a simpler syntactical series of filters.
	 * @return Ambigous
	 */
	public function show( $repo_slug, $account_name = null, IssueFilter $filter = null)
	{
		$response = null;
		$filter_string = null;

		if (!empty($filter))
		{
			$filter_string = (string)$filter;

		}
		else
		{
			if (!empty($this->filter))
			{
				$filter_string = (string)$this->filter;
			}
		}

		$this->checkUsername( $account_name );
		Helper::format_slug( $repo_slug );
		$response = $this->api->get( "/repositories/{$account_name}/{$repo_slug}/issues", $filter_string );

		return $response;
	}

	/**
	 * Gets in individual issue from a repository. Authorisation is not required for public repositories with a public issue tracker. Private repositories or private issue trackers require the caller to authenticate  with an account that has appropriate access.
	 * @param string $repo_slug		The repository identifier.
	 * @param int $issue_id			The issue identifier
	 * @param string $account_name	The team or individual account owning the repository.
	 */
	public function show_single($repo_slug, $issue_id, $account_name = null)
	{
		$response = null;

		$this->checkUsername( $account_name );
		Helper::format_slug( $repo_slug );
		$response = $this->api->get( "/repositories/{$account_name}/{$repo_slug}/issues/{$issue_id}" );

		return $response;
	}

	/**
	 * Gets the followers for an individual issue from a repository. authorisation is not required for public repositories with a public issue tracker. Private repositories or private issue trackers require the caller to authenticate  with an account that has appropriate access.
	 * @param string $repo_slug		The repository identifier.
	 * @param int $issue_id			The issue identifier
	 * @param string $account_name	The team or individual account owning the repository.
	 * @return \bitbucket\api\Ambigous
	 */
	public function followers($repo_slug, $issue_id, $account_name = null)
	{
		$response = null;

		$this->checkUsername($account_name);
		Helper::format_slug($repo_slug);
		$response = $this->api->get("/repositories/{$account_name}/{$repo_slug}/issues/{$issue_id}/followers");

		return $response;
	}

	/**
	 * Creates a new issue in a repository. This call requires authentication. Private repositories or private issue trackers require the caller to authenticate with an account that has appropriate authorisation. The authenticated user is used for the issue's reported_by field.
	 * Example:
	 * <code>
	 * <?php
	 * $api = new Api( $username, $password );
	 *
	 * $res = $api->repositories->issues->fields
	 * 	->title("test")
	 * 	->status(Status::STATUS_NEW)
	 * 	->content("Issue description")
	 * 	->create("cateinthehat", "drseuss");
	 *
     * print_r( $res );
	 * ?>
	 * </code>
	 * @param string $repo_slug		The repository identifier.
	 * @param int $issue_id			The issue identifier
	 * @param Fields $fields		Fields object substantiated before invoking this method
	 * @return \bitbucket\api\Ambigous
	 */
	public function create( $repo_slug, $account_name = null, IssueFields $fields = null )
	{
		$response = null;
		$fields_string = "";

		if (!empty($fields))
		{
			$fields_string = (string)$fields;

		}
		else
		{
			if (!empty($this->fields))
			{
				$fields_string = (string)$this->fields;
			}
		}

		$this->checkUsername($account_name);
		Helper::format_slug($repo_slug);
		$response = $this->api->post("/repositories/{$account_name}/{$repo_slug}/issues", $fields_string);

		return $response;
	}

	public function update()
	{

	}
}
