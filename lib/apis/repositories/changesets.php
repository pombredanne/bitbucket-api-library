<?php
/**
 * PHP 5.3 Bitbucket Api Library
 *
 * @copyright 2012 Provisionists, LLC
 * @license MIT
 * @version v0.1.6-rc
 * @author Anthony Steiner <asteiner@provisionists.com>
 *
 */
namespace bitbucket\api\repositories;

use \bitbucket\api\Helper;
use \bitbucket\api\Api;
use \bitbucket\api\ApiBase;

/**
 * Use changesets resources to manage changesets resources on a repository. Unauthenticated calls for these resources only return values for public repositories. To see changeset resources on private repositories, the caller must authenticated and must have at least read permissions on the repository. Changesets are read-only resources, you can't add or modify a changeset structure. You can modify the secondary resources such as likes and comments associated with an individual change set node.
 *
 * @author Anthony Steiner <asteiner@provisionists.com>
 * @package Bitbucket Api Library
 */
class Changesets extends ApiBase
{
	/**
	 * Gets a list of change sets associated with a repository. By default, this call returns the 15 most recent changesets. It also returns the count which is the total number of changesets on the repository. Private repositories require the caller to authenticate.
	 * @param string $repo_slug		The repo identifier.
	 * @param string $limit			An integer representing how many changesets to return. You can specify a limit between 0 and 50. If you specify 0 (zero), the system returns the count but returns empty values for the remaining fields.
	 * @param string $start			A hash value representing the earliest node to start with. The system starts with the specified node and includes the older requests that preceded it. The Bitbucket GUI lists the nodes on the Commit tab. The default start value is the tip.
	 * @param string $account_name	The team or individual account owning the repo.
	 * @return \bitbucket\api\Ambigous
	 */
	public function show($repo_slug, $limit = 50, $start = 'tip', $account_name = null)
	{
		$response = null;
		$data = array();

		$this->checkUsername($account_name);
		Helper::format_slug($repo_slug);

		if ($limit < 0)
		{
			$limit = 0;
		}
		elseif($limit > 50)
		{
			$limit = 50;
		}
		else
		{
			$limit = 50;
		}

		$data = array('limit' => $limit, 'start' => $start);

		$response = $this->api->get( "/repositories/{$account_name}/{$repo_slug}/changesets", $data);

		return $response;
	}

	/**
	 * Returns an array containing statistics on changed file associated with a particular node in a change set. Private repositories require the caller to authenticate.
 	 * @param string $repo_slug		The repo identifier.
	 * @param string $raw_node		The raw node change identifier
	 * @param string $account_name	The team or individual account owning the repo.
	 * @return \bitbucket\api\Ambigous
	 */
	public function show_single($repo_slug, $raw_node, $account_name = null)
	{
		$response = null;

		$this->checkUsername($account_name);
		Helper::format_slug($repo_slug);

		$response = $this->api->get( "/repositories/{$account_name}/{$repo_slug}/changesets/{$raw_node}");

		return $response;
	}

	/**
	 * Returns an array containing statistics on changed file associated with a particular node in a change set. Private repositories require the caller to authenticate.
 	 * @param string $repo_slug		The repo identifier.
	 * @param string $raw_node		The raw node change identifier
	 * @param string $account_name	The team or individual account owning the repo.
	 * @return \bitbucket\api\Ambigous
	 */
	public function statistics($repo_slug, $raw_node, $account_name = null)
	{
		$response = null;

		$this->checkUsername($account_name);
		Helper::format_slug($repo_slug);

		$response = $this->api->get( "/repositories/{$account_name}/{$repo_slug}/changesets/{$raw_node}/diffstat");

		return $response;
	}

	/**
	 * Gets the actual diff associated with the changeset node. This call returns the output as a string containing JSON. Private repositories require the caller to authenticate.
 	 * @param string $repo_slug		The repo identifier.
	 * @param string $raw_node		The raw node change identifier
	 * @param string $account_name	The team or individual account owning the repo.
	 * @return \bitbucket\api\Ambigous
	 */
	public function diff($repo_slug, $raw_node, $account_name = null)
	{
		$response = null;

		$this->checkUsername($account_name);
		Helper::format_slug($repo_slug);

		$response = $this->api->get( "/repositories/{$account_name}/{$repo_slug}/changesets/{$raw_node}/diff");

		return $response;
	}

	/**
	 * Gets the set of likes associated with a changeset. Users can like a particular changeset to indicate their approval. Private repositories require the caller to authenticate.
 	 * @param string $repo_slug		The repo identifier.
	 * @param string $raw_node		The raw node change identifier
	 * @param string $account_name	The team or individual account owning the repo.
	 * @return \bitbucket\api\Ambigous
	 */
	public function likes($repo_slug, $raw_node, $account_name = null)
	{
		$response = null;

		$this->checkUsername($account_name);
		Helper::format_slug($repo_slug);

		$response = $this->api->get( "/repositories/{$account_name}/{$repo_slug}/changesets/{$raw_node}/likes");

		return $response;
	}

	/**
 	 * @param string $repo_slug		The repo identifier.
	 * @param string $raw_node		The raw node change identifier
	 * @param string $liker_account	The team or individual account owning the repo.
	 * @param string $account_name	The team or individual account owning the repo.
	 * @return \bitbucket\api\Ambigous
	 */
	public function likes_by_account($repo_slug, $raw_node, $liker_account = null, $account_name = null)
	{
		$response = null;

		$this->checkUsername($account_name);
		$this->checkUsername($liker_account);
		Helper::format_slug($repo_slug);

		$response = $this->api->get( "/repositories/{$account_name}/{$repo_slug}/changesets/{$raw_node}/likes/{$liker_account}");

		return $response;
	}

	/**
	 * Gets the like from a particular accountname. Users can like a particular changeset to indicate their approval. Private repositories require the caller to authenticate.
	 * @param string $repo_slug		The repo identifier.
	 * @param string $raw_node		The raw node change identifier
	 * @param string $account_name	The team or individual account owning the repo.
	 * @return \bitbucket\api\Ambigous
	 */
	public function comments($repo_slug, $raw_node, $account_name = null)
	{
		$response = null;

		$this->checkUsername($account_name);
		Helper::format_slug($repo_slug);

		$response = $this->api->get( "/repositories/{$account_name}/{$repo_slug}/changesets/{$raw_node}/comments");

		return $response;
	}

	/**
	 * Deletes the specified comment_id associated with a particular changeset node. To delete a comment, you must have administrative rights on the repository, the account, or be authenticated as the username associated with the comment. Private repositories require the caller to authenticate.
	 * @param string $repo_slug		The repo identifier.
	 * @param string $raw_node		The raw node change identifier
	 * @param string $comment_id	The comment identifier.
	 * @param string $account_name	The team or individual account owning the repo.
	 * @return \bitbucket\api\Ambigous
	 */
	public function delete_comment($repo_slug, $raw_node, $comment_id, $account_name = null)
	{
		$response = null;

		$this->checkUsername( $account_name );
		Helper::format_slug($repo_slug);

		$response = $this->api->delete( "/repositories/{$account_name}/{$repo_slug}/changesets/{$raw_node}/comments/{$comment_id}" );
		//$response = $this->api->getRequest()->http_code == '204' ? true : false;

		return $response;
	}

	/**
	 * Adds a new comment to a changeset node. This call requires authentication. The call can add a new comment or create a comment in reply to another specified by the parent_id parameter. The system creates a comment on behalf of the authenticated caller. The authenticated caller must have administrative or write rights on the repository to POST a new comment.
	 * Private repositories require the caller to authenticate.
	 *
	 * @param string $repo_slug		The repo identifier.
	 * @param string $raw_node		The raw node change identifier
	 * @param string $comment_id	The comment identifier.
	 * @param string $content		A Striing containing the content of the comment.
	 * @param int $line_from		An integer representing the starting line of the comment.
	 * @param int $line_to			An integer representing the ending line of the comment.
	 * @param int $parent_id		An integer representing the unique ID of comment to which this is a reply.
	 * @param string $filename		A String representing a filename in the changeset to which this comment applies.
 	 * @param string $account_name	The team or individual account owning the repo.
	 * @return \bitbucket\api\Ambigous
	 */
	public function create_comment($repo_slug, $raw_node, $comment_id, $content, $line_from = null, $line_to = null, $parent_id = null, $filename = null, $account_name = null)
	{
		$response = null;
		$data = array();

		$this->checkUsername( $account_name );
		Helper::format_slug($repo_slug);

		$data = array(
					'content'	=> $content,
					'line_form'	=> $line_from,
					'line_to'	=> $line_to,
					'parent_id'	=> $parent_id,
					'filename'	=> $filename
				);

		$data = array_filter($data);

		$response = $this->api->post( "/repositories/{$account_name}/{$repo_slug}/changesets/{$raw_node}/comments", $data );

		return $response;
	}

	/**
	 * Puts an update to an existing changeset comment identified by the comment_id. This call requires authentication. The system updates a comment on behalf of the authenticated caller. The caller must authenticate as a user with administrative privileges on the account, the repo, or as the user that created the comment. Private repositories require the caller to authenticate.
	 * @param string $repo_slug		The repo identifier.
	 * @param string $raw_node		The raw node change identifier
	 * @param string $content		A Striing containing the content of the comment.
	 * @param int $line_from		An integer representing the starting line of the comment.
	 * @param int $line_to			An integer representing the ending line of the comment.
	 * @param int $parent_id		An integer representing the unique ID of comment to which this is a reply.
	 * @param string $filename		A String representing a filename in the changeset to which this comment applies.
 	 * @param string $account_name	The team or individual account owning the repo.
	 * @return \bitbucket\api\Ambigous
	 */
	public function update_comment($repo_slug, $raw_node, $content, $line_from = null, $line_to = null, $parent_id = null, $filename = null, $account_name = null)
	{
		$response = null;
		$data = array();

		$this->checkUsername( $account_name );
		Helper::format_slug($repo_slug);

		$data = array(
				'content'	=> $content,
				'line_form'	=> $line_from,
				'line_to'	=> $line_to,
				'parent_id'	=> $parent_id,
				'filename'	=> $filename
		);

		$data = array_filter($data);

		$response = $this->api->post( "/repositories/{$account_name}/{$repo_slug}/changesets/{$raw_node}/comments", $data );

		return $response;
	}

	/**
	 * Toggles the spam flag on a changeset comment identified by the comment_id. This call requires authentication.
	 * @param string $repo_slug		The repo identifier.
	 * @param string $raw_node		The raw node change identifier
	 * @param string $comment_id	The comment identifier.
	 * @param string $account_name	The team or individual account owning the repo.
	 * @return \bitbucket\api\Ambigous
	 */
	public function flag_comment_spam_toggle($repo_slug, $raw_node, $comment_id, $account_name = null)
	{
		$response = null;

		$this->checkUsername($account_name);
		Helper::format_slug($repo_slug);

		$response = $this->api->put( "/repositories/{$account_name}/{$repo_slug}/changesets/{$raw_node}/comments/spam/{$comment_id}");

		return $response;
	}
}