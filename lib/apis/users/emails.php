<?php
/**
 * PHP 5.3 Bitbucket Api Library
 *
 * @copyright 2012 Provisionists, LLC
 * @license MIT
 * @version v0.1.6-rc
 * @author Anthony Steiner <asteiner@provisionists.com>
 *
 */
namespace bitbucket\api\users;

use bitbucket\api\Api;
use bitbucket\api\ApiBase;

/**
 * An account can have one or more email addresses associated with it. Use this end point to list, change, or create an email address.
 *
 * @author Anthony Steiner <asteiner@provisionists.com>
 * @package Bitbucket Api Library
 */
class Emails extends ApiBase
{

    /**
     * Gets the email addresses associated with the account.
     * This call requires authentication.
     * The possible return fields are the same for both individual and team accounts
     *
     * @param string $account_name The name of an individual or team account.
     * @return Ambigous <\bitbucket\api\Ambigous, object, mixed>
     */
    public function show( $account_name = null )
    {
        $response = null;

        $this->checkUsername( $account_name );

        $response = $this->api->get( "/users/{$account_name}/emails/" );

        return $response;
    }

    /**
     * Gets an individual email address associated with an account. This call requires authentication.
     *
     * @param string $email The email address to get.
     * @param string $account_name The name of an individual or team account.
     * @return Ambigous <\bitbucket\api\Ambigous, object, mixed>
     */
    public function show_email( $email, $account_name = null )
    {
        $response = null;

        $this->checkUsername( $account_name );

        $response = $this->api->get( "/users/{$account_name}/emails/{$email}/" );

        return $response;
    }

    /**
     * Adds additional email addresses to an account.
     * This call requires authentication.
     * After an account has multiple email addresses, you can use the PUT call below to set one of the addresses to primary.
     *
     * @param string $email The email address to get.
     * @param string $account_name The name of an individual or team account.
     * @return Ambigous <\bitbucket\api\Ambigous, object, mixed>
     */
    public function add_email( $email, $account_name = null )
    {
        $response = null;

        $this->checkUsername( $account_name );

        $response = $this->api->post( "/users/{$account_name}/emails/{$email}/", array('email' => $email));

        return $response;
    }

    /**
     * Sets an individual email address associated with an account to primary.
     * The primary email address is the main email contact for the account.
     * Only a single address on an account can be primary.
     * If another address had primary set prior to this call, after it is no longer primary.
     * This call requires authentication.
     *
     * @param string $email The email address to get.
     * @param string $account_name The name of an individual or team account.
     * @return Ambigous <\bitbucket\api\Ambigous, object, mixed>
     */
    public function set_primary( $email, $account_name = null )
    {
        $response = null;

        $this->checkUsername( $account_name );

        $response = $this->api->put( "/users/{$account_name}/emails/{$email}/", array(
            'primary' => true
        ) );

        return $response;
    }
}