<?php
/**
 * PHP 5.3 Bitbucket Api Library
 *
 * @copyright 2012 Provisionists, LLC
 * @license MIT
 * @version v0.1.6-rc
 * @author Anthony Steiner <asteiner@provisionists.com>
 *
 */
namespace bitbucket\api\users;

use bitbucket\api\ApiBase;
use bitbucket\api\Api;

/**
 * Use the oauth resource to create your own OAuth consumers. Oauth is an open standard for authorization. Oauth allows you to use Bitbucket a service provider. You can create a consumer with this API or using the OAuth Consumer option on the Account > Integrated Applications page. When creating consumers, Bitbucket generates a unique consumer key and secret for you. You cannot provide or change these yourself.
 *
 * @author Anthony Steiner <asteiner@provisionists.com>
 * @package Bitbucket Api Library
 *
 */
class Oauth extends ApiBase
{

    /**
     * Gets the OAuth consumers currently configured on user's account. This call requires authentication. The caller must authenticate as the owner for an individual account or  as a user with administrative rights for a team account.
     * @param string $account_name The name of an individual or team account.

     * @return Ambigous <\bitbucket\api\Ambigous, object, mixed>
     */
    public function show( $account_name = null )
    {
        $response = null;

        $this->checkUsername( $account_name );

        $response = $this->api->get( "/users/{$account_name}/consumers" );

        return $response;
    }

    /**
     * Creates a new OAuth consumer for an individual or team account. The caller must authenticate as the owner for an individual account or  as a user with administrative rights for a team account. This will return a 201 status code on success, with the contents of the new consumer. When creating consumers, Bitbucket will generate a unique consumer key and secret for you. You cannot provide or change these yourself.
     * @param string $name			A display name for the key.
     * @param string $description	A description of the key.
     * @param string $site_url		The location of the service that will use the key.
     * @param string $account_name	The name of an individual or team account.
     * @return Ambigous <\bitbucket\api\Ambigous, object, mixed>
     */
    public function create( $name, $description, $site_url, $account_name = null )
    {
        $response = null;
        $data = array();

        $this->checkUsername( $account_name );
        $data = array(
            'name' => $name, 'description' => $description, 'url' => $site_url
        );

        $response = $this->api->post( "/users/{$account_name}/consumers" );

        return $response;
    }

    /**
     * Updates an existing OAuth consumer for an individual or team account. The caller must authenticate as the owner for an individual account or as a user with administrative rights for a team account. When creating consumers, Bitbucket will generate a unique consumer key and secret for you. You cannot provide or change these yourself.
     * @param string $key_id		The id of the key to update.
     * @param string $name			A display name for the key.
     * @param string $description	A description of the key.
     * @param string $site_url		The location of the service that will use the key.
     * @param string $account_name	The name of an individual or team account.
     * @return Ambigous <\bitbucket\api\Ambigous, object, mixed>
     */
    public function update( $key_id, $name, $description = null, $site_url = null, $account_name = null )
    {
        $response = null;
        $data = array();

        $this->checkUsername( $account_name );
        $data = array(
            'name' => $name, 'description' => $description, 'url' => $site_url
        );

        $response = $this->api->post( "/users/{$account_name}/consumers/{$key_id}" );

        return $response;
    }

    /**
     * Deletes an existing OAuth consumer for an individual or team account. The caller must authenticate as the owner for an individual account or as a user with administrative rights for a team account.
     * @param string $key_id		The id of the key to update.
     * @param string $account_name 	The name of an individual or team account.
     * @return boolean
     */
    public function delete( $key_id, $account_name = null )
    {
        $response = null;

        $this->checkUsername( $account_name );

        $this->api->delete( "/users/{$account_name}/consumers/{$key_id}" );
        $response = $this->api->getRequest()->http_code == '204' ? true : false;

        return $response;
    }
}