<?php
/**
 * PHP 5.3 Bitbucket Api Library
 *
 * @copyright 2012 Provisionists, LLC
 * @license MIT
 * @version v0.1.6-rc
 * @author Anthony Steiner <asteiner@provisionists.com>
 *
 */
namespace bitbucket\api;

/**
 * The /users endpoints gets information related to an individual or team account.
 * Individual and team accounts both have the same set of user fields
 *
 * @author Anthony Steiner <asteiner@provisionists.com>
 * @package Bitbucket Api Library
 */
class Users extends Api
{
    /**
     * Gets information related to an account.
     *
     * @var \bitbucket\api\users\Account
     */
    public $account;

    /**
     * Manages the email address associated with an account.
     *
     * @var \bitbucket\api\users\Emails
     */
    public $emails;

    /**
     * Manages the invitations sent by an account.
     *
     * @var \bitbucket\api\users\Invitations
     */
    public $invitations;

    /**
     * Manages the consumers associated with an account.
     *
     * @var \bitbucket\api\users\Oauth
     */
    public $oauth;

    /**
     * Manages privilege settings for team accounts.
     *
     * @var \bitbucket\api\users\Privileges
     */
    public $privileges;

    /**
     * Manages ssh keys for an account.
     *
     * @var \bitbucket\api\users\SshKeys
     */
    public $ssh_keys;

	/**
	 * Extended Constructor, forces a pass-thru context
	 */
	public function __construct(Api $api)
	{
	    parent::__construct(
    	        $api->getOption("username"),
    	        $api->getOption("password"),
    	        $api->getOption("format", Api_format::OBJECT),
    	        $api->debug
	        );

	}

    /**
	 * Assign all APIs to their respective chain-segment
	 */
	public function assign_apis()
	{
		$refclass = new \ReflectionClass( $this );
		foreach( $refclass->getProperties(\ReflectionProperty::IS_PUBLIC) as $property )
		{
			$name = $property->name;
			if ( $property->class == $refclass->name )
			{
				$by_ref = explode("\\", __CLASS__);
				$this->{$property->name} = $this->getApi(end($by_ref), $property->name);
			}
		}
	}

}
