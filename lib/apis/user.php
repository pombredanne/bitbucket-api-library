<?php
/**
 * PHP 5.3 Bitbucket Api Library
 *
 * @copyright 2012 Provisionists, LLC
 * @license MIT
 * @version v0.1.6-rc
 * @author Anthony Steiner <asteiner@provisionists.com>
 */
namespace bitbucket\api;


/**
 * Use the /user endpoints to gets information related to the currently authenticated user.
 * It is useful for OAuth or other in situations where the username is unknown.
 * This endpoint returns information about an individual or team account.
 *
 * @author Anthony Steiner <asteiner@provisionists.com>
 * @package Bitbucket Api Library
 */
class User extends ApiBase
{

    /**
     * Gets the basic information associated with an account and a list of all its repositories both public and private.
     *
     * @return Ambigous <\bitbucket\api\Ambigous, object, mixed>
     */
    public function show()
    {
        $username = null;
        $response = null;

        $this->checkUsername( $username );
        $response = $this->api->get( "/user/" );

        return $response;
    }

    /**
     * Updates the basic information associated with an account.
     * It operates on the currently authenticated user.
     * Specify one or more fields with the exception of the username field.
     * This call ignores changes to the username field. Omitted fields retain their existing values. The call returns the user structure.
     *
     * @param array $fields_array
     * @return Ambigous <\bitbucket\api\Ambigous, object, mixed>
     */
    public function update( array $fields_array )
    {
        $username = null;
        $response = null;

        $this->checkUsername( $username );
        $response = $this->api->put( "/user/", $fields_array );

        return $response;
    }


    /**
     * Gets the account-level privileges for an individual or team account.
     * Use this call to locate the accounts that the currently authenticated accountname has access to.
     * An account can have admin or collaborator (member) privileges. The accountname always has admin privileges on itself.
     *
 	 * @return Ambigous <\bitbucket\api\Ambigous, object, mixed>
     */
    public function privileges()
    {
        $username = null;
        $response = null;

        $this->checkUsername( $username );
        $response = $this->api->get( "/user/privileges/" );

        return $response;
    }

    /**
     * Gets the details of the repositories that the individual or team account follows.
     * This call returns the full data about the repositories including if the repository is a fork of another repository.
     * An account always "follows" its own repositories.
     *
     * @return Ambigous <\bitbucket\api\Ambigous, object, mixed>
     */
    public function follows()
    {
        $username = null;
        $response = null;

        $this->checkUsername( $username );
        $response = $this->api->get( "/user/follows/" );

        return $response;
    }

    /**
     * Gets the repositories owned by the individual or team account.
     *
     * @return Ambigous <\bitbucket\api\Ambigous, object, mixed>
     */
    public function repositories()
    {
        $username = null;
        $response = null;

        $this->checkUsername( $username );
        $response = $this->api->get( "/user/repositories/" );

        return $response;
    }

    /**
     * Gets a list of the repositories the account follows.
     *
     * @return Ambigous <\bitbucket\api\Ambigous, object, mixed>
     */
    public function overview()
    {
        $username = null;
        $response = null;

        $this->checkUsername( $username );
        $response = $this->api->get( "/user/repositories/overview/" );

        return $response;
    }

    /**
     * Gets the repositories list from the account's dashboard.
     *
     * @return Ambigous <\bitbucket\api\Ambigous, object, mixed>
     */
    public function dashboard()
    {
        $username = null;
        $response = null;

        $this->checkUsername( $username );
        $response = $this->api->get( "/user/repositories/dashboard/" );

        return $response;
    }

    /**
     * The newuser endpoint is a POST only resource.
     * Use this resource to create an individual account on the Bitbucket service.
     * The service throttles this call at 2 request per 30 minutes. The caller need not authenticate.
     * @see bitbucket\api\Newuser::create()
     *
     * @param string	$display_name	The name of the user.
     * @param string	$email			The email address of the new user.
     * @param string	$username		The team or individual account name. You can supply a user name or a valid email address.
     * @param string	$password		The user's password.
     * @return Request
     */
    public function create($display_name, $email, $username, $password)
    {
        return $this->api->newuser($display_name, $email, $username, $password);
    }
}
