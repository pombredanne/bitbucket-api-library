<?php
/**
 * PHP 5.3 Bitbucket Api Library
 *
 * @copyright 2012 Provisionists, LLC
 * @license MIT
 * @version v0.1.6-rc
 * @author Anthony Steiner <asteiner@provisionists.com>
 */
namespace bitbucket\api;

require_once 'lib/api.php';

/**
 * Use the privileges endpoint to manage the user privileges (permissions) of your repositories.
 * It allows you to grant specific users access to read, write and or administer your repositories.
 * Only the repository owner, a team account administrator, or an account with administrative
 * rights on the repository can can query or modify repository privileges.
 * To manage group access to your repositories, use the group-privileges Endpoint and to
 * manage privilege settings for team accounts, use the users-privileges Resource.
 *
 * @author Anthony Steiner <asteiner@provisionists.com>
 * @package Bitbucket Api Library
 */
class Privileges extends ApiBase
{

	/**
	 * Gets a list of all the privilege across all an account's repositories. If a repository has no individual users with privileges, it does not appear in this list. Only the repository owner, a team account administrator, or an account with administrative rights on the repository can make this call.
	 * @param string $account_name		Owner of the repository.
	 * @return \bitbucket\api\Ambigous
	 */
	public function show( $account_name = null )
	{
		$response = null;

		$this->checkUsername( $account_name );

		$response = $this->api->get( "/privileges/{$account_name}" );

		return $response;
	}

	/**
	 * Gets a list of the privileges granted on a repository.  Only the repository owner, a team account administrator, or an account with administrative rights on the repository can make this call. If a repository has no individual users with privileges, it does not appear in this list.
	 * @param string $repo_slug			Repository identifier.
	 * @param string $account_name		Owner of the repository.
	 * @return \bitbucket\api\Ambigous
	 */
	public function show_by_repository( $repo_slug, $account_name = null )
	{
		$response = null;

		$this->checkUsername( $account_name );
		Helper::format_slug( $repo_slug );

		$response = $this->api->get( "/privileges/{$account_name}/{$repo_slug}" );

		return $response;
	}


	/**
	 * Get a list of privileges for an individual account. Only the repository owner, a team account administrator, or an account with administrative rights on the repository can make this call.
	 * @param string $privilege_account	The account to list privileges for.
	 * @param string $repo_slug			Repository identifier.
	 * @param string $account_name		Owner of the repository.
	 * @return \bitbucket\api\Ambigous
	 */
	public function show_by_account( $privilege_account, $repo_slug, $account_name = null )
	{
		$response = null;

		$this->checkUsername( $account_name );
		Helper::format_slug( $repo_slug );

		$response = $this->api->get( "/privileges/{$account_name}/{$repo_slug}/{$privilege_account}" );

		return $response;
	}

	/**
	 * Grants an account a privilege on a repository. You can upgrade or downgrade a user's permissions with this method. Only the repository owner, a team account administrator, or an account with administrative rights on the repository can make this call.
	 * @param string $privilege_account	The account to list privileges for.
	 * @param string $repo_slug			Repository identifier.
	 * @param string $privilege			The privilege to assign. Valid values are 'read', 'write', 'admin'. Use {@see bitbucket\api\privilege_enum} for best results
	 * @param string $account_name		Owner of the repository.
	 * @return boolean
	 */
	public function create( $privilege_account, $repo_slug, $privilege = privilege_enum::READ, $account_name = null )
	{
		$response = null;
		$data = array( );

		$this->checkUsername( $account_name );
		privilege_enum::check( $privilege );
		Helper::format_slug( $repo_slug );

		$data = $privilege;

		$this->api->put( "/privileges/{$account_name}/{$repo_slug}/{$privilege_account}", $data );
		$response = $this->api->getRequest( )->http_code == '200' ? true : false;

		return $response;
	}


	/**
	 * Delete a privileges from all repositories. Only the repository owner, a team account administrator, or an account with administrative rights on the repository can make this call.
	 * @param string $account_name Owner of the repositories.
	 * @return boolean
	 */
	public function delete( $account_name = null )
	{
		$response = null;
		$this->checkUsername( $account_name );


		$this->api->delete( "/privileges/{$account_name}" );
		$response = $this->api->getRequest( )->http_code == '204' ? true : false;

		return $response;
	}

	/**
	 * Delete an account's privileges from a repository. Only the repository owner, a team account administrator, or an account with administrative rights on the repository can make this call.
	 * @param string $privilege_account	The account to list privileges for.
	 * @param string $repo_slug			Repository identifier.
	 * @param string $account_name		Owner of the repository.
	 * @return boolean
	 */
	public function delete_account_privileges( $privilege_account, $repo_slug, $account_name = null )
	{
		$response = null;
		$this->checkUsername( $account_name );

		Helper::format_slug( $repo_slug );

		$this->api->delete( "/privileges/{$account_name}/{$repo_slug}/{$privilege_account}" );
		$response = $this->api->getRequest( )->http_code == '204' ? true : false;

		return $response;
	}

	/**
	 * Delete all privileges from a repository. Only the repository owner, a team account administrator, or an account with administrative rights on the repository can make this call.
	 * @param string $repo_slug			Repository identifier.
	 * @param string $account_name		Owner of the repository.
	 * @return boolean
	 */
	public function delete_all_privileges( $repo_slug, $account_name = null )
	{
		$response = null;
		$this->checkUsername( $account_name );

		Helper::format_slug( $repo_slug );

		$this->api->delete( "/privileges/{$account_name}/{$repo_slug}" );
		$response = $this->api->getRequest( )->http_code == '204' ? true : false;

		return $response;
	}

}
