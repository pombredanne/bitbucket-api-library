<?php
/**
 * PHP 5.3 Bitbucket Api Library
 *
 * @copyright 2012 Provisionists, LLC
 * @license MIT
 * @version v0.1.6-rc
 * @author Anthony Steiner <asteiner@provisionists.com>
 */
namespace bitbucket\api;

require_once 'lib/api.php';

/**
 * The newuser endpoint is a POST only resource.
 * Use this resource to create an individual account on the Bitbucket service.
 * The service throttles this call at 2 request per 30 minutes. The caller need not authenticate.
 *
 * @author Anthony Steiner <asteiner@provisionists.com>
 * @package Bitbucket Api Library
 */
class Newuser extends ApiBase
{
    /**
     * The newuser endpoint is a POST only resource.
 	 * Use this resource to create an individual account on the Bitbucket service.
 	 * The service throttles this call at 2 request per 30 minutes. The caller need not authenticate.
 	 *
 	 * @param string	$display_name	The name of the user.
     * @param string	$email			The email address of the new user.
     * @param string	$username		The team or individual account name. You can supply a user name or a valid email address.
     * @param string	$password		The user's password.
     * @return Request
     */
    public function create($display_name, $email, $username, $password)
    {
        $response = null;
        $data = array();

        $data = array(
            'username' => $username,
            'name' => $display_name,
            'password' => $password,
            'email' => $email
            );

        $response = $this->api->post("newuser", $data);

        return $response;
    }
}